/**
 * @(#)SnakeMainGame.java
 * @author William Yen
 * @version 1.00 2024/4/17
 */


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class Main extends JPanel {

    static final String TITLE = "Snake game JAVA 2d";
    static final int ROWS = 40;
    static final int COLUMNS = 40;
    static final int CELL_SIZE = 15;
    static final int CANVAS_WIDTH = COLUMNS * CELL_SIZE;
    static final int CANVAS_HEIGHT = ROWS * CELL_SIZE;
    //speed of the snake
    static final int UPDATE_PER_SEC = 15;
    static final long UPDATE_PERIOD_NSEC = 1000000000L / UPDATE_PER_SEC;
    /**
     * Creates a new instance of <code>SnakeMainGame</code>.
     */
    private static final long serialVersionUID = 1L;
    //Declare menubar
    static JMenuBar menuBar;
    // current state
    static GameState state;
    //time variables
    private final long startTime;
    private final GameBoard pit;
    private final ControlPanel control;
    int score = 0;
    private long elapsedTime;
    private Fruit fruit;
    private Snake snake;
    private JLabel lblScore;

    public Main(String playerName) {
        //initialize game objects
        gameInit();


//UI COMPONENTS
        setLayout(new BorderLayout());
        //drawingh the panel
        pit = new GameBoard();
        pit.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
        add(pit, BorderLayout.CENTER);

        //JTextField for user name
        JTextField playerNameField = new JTextField(10);
        playerNameField.setFont(new Font("Georgia", Font.PLAIN, 10));

        //control panel
        control = new ControlPanel(playerName, snake);
        add(control, BorderLayout.SOUTH);


        //add menu bar
        setupMenuBar();


        //start game
        gameStart();

        startTime = System.nanoTime();
    }
// CHANGE MAIN PANEL
    private static String getPlayerName() {
        JTextField playerNameField = new JTextField(15);
        JPanel panelName = new JPanel();
        panelName.setSize(600,600);
        Calender1 c = new Calender1();
        int hr = c.getHour();
        JPanel morning = new Morning();
        JPanel day = new Day();
        JPanel evening = new Evening();

        if(hr < 12)
        {
            panelName.add(morning);
        }

        else if(hr < 4)
        {
            panelName.add(day);

        }

        else
        {
            panelName.add(evening);

        }


        panelName.setLayout(new BoxLayout(panelName, BoxLayout.Y_AXIS));



        //panel.add(new JLabel("Enter username: "));
        panelName.add(playerNameField);
        int result = JOptionPane.showConfirmDialog(null, panelName, "Username", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION && !playerNameField.getText().isEmpty()) {
            return playerNameField.getText();
        }
        return null;
    }

    //main function
    public static void main(String[] args) {
        String playerName = getPlayerName();
        //event dispatch thread for UI , thread safety
        if (playerName != null) {


            SwingUtilities.invokeLater(() -> {
                JFrame frame = new JFrame(TITLE);
                Main game = new Main(playerName);
                //main JPanel as content pane
                frame.setContentPane(game);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                //center app window
                frame.setLocationRelativeTo(null);
                frame.setJMenuBar(menuBar);
                frame.setVisible(true);

            });
        } else {
            JOptionPane.showMessageDialog(null, "youre a bot.", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    //-------------
    public void gameInit() {
        snake = new Snake();
        fruit = new Fruit();
        state = GameState.INITIALIZED;
    }

    // turn off game, clean uop code
    public void gameShutDown() {


    }

    //stop, restart game
    public void gameStart() {
        //creat new thread
        Thread gameThread = new Thread(this::gameLoop);
        //start gamethread, calls run method, then calls gameLoop
        gameThread.start();
    }

    //gameloop run
    private void gameLoop() {
        //Regenerate and reset the game objects for a new game
        if (state == GameState.INITIALIZED || state == GameState.GAMEOVER) {
            //Generate a new snake and a fruit item
            snake.regenerate();


            //regenerate if fruit placed under the snake
            int x, y;
            do {
                fruit.regenerate();
                x = fruit.getX();
                y = fruit.getY();
            } while (snake.contains(x, y));


            state = GameState.PLAYING;


        }
        //Game loop
        long beginTime, timeTaken, timeLeft; //in msec
        while (state != GameState.GAMEOVER) {
            beginTime = System.nanoTime();

            if (state == GameState.PLAYING) {
                //update the state and position of all the game objects
                //detect collisions and provide responses
                gameUpdate();
                //timer on game; if paused, will stop
                elapsedTime = (System.nanoTime() - startTime) / 1000000000;
                updateTimerLabel();
            }
            // Refresh the display
            repaint();
            //Delay timer to provide the necessary delay to meet the target rate
            timeTaken = System.nanoTime() - beginTime;
            // in milliseconds
            timeLeft = (UPDATE_PERIOD_NSEC - timeTaken) / 1000000;
            if (timeLeft < 10) timeLeft = 10; //set a minium
            try {

                Thread.sleep(timeLeft);
            } catch (InterruptedException ex) {
            }


        }
    }

    public void updateTimerLabel() {
        SwingUtilities.invokeLater(() -> control.timerLabel.setText("Time Elapsed: " + elapsedTime + " seconds"));
    }

    //update state and position of game objects, detech collisions
    public void gameUpdate() {
        snake.update();
        processCollision();
    }

    //detect collision and rresponse
    public void processCollision() {
        // check if this snake eats the fruit item
        int headX = snake.getHeadX();
        int headY = snake.getHeadY();

        if (headX == fruit.getX() && headY == fruit.getY()) {
            // to play a specific sound
            SoundEffect.EAT.play();
            score = score + 1;
            lblScore.setText("Score: " + score);

            //fruit eaten, regenerate one
            int x, y;
            do {
                fruit.regenerate();
                x = fruit.getX();
                y = fruit.getY();
            } while (snake.contains(x, y));
        } else {
            //not eaten, shrink the tail
            snake.shrink();
        }

        // Check if the snake moves out of bounds
        if (!pit.contains(headX, headY)) {
            state = GameState.GAMEOVER;
            // to play a specific sound
            SoundEffect.DIE.play();
            score = 0;
            lblScore.setText("Score: " + score);
            return;
        }

        // Check if the snake eats itself
        if (snake.eatItself()) {
            state = GameState.GAMEOVER;
            // to play a specific sound
            SoundEffect.DIE.play();
            score = 0;
            lblScore.setText("Score: " + score);
        }
    }

    //refresh display, calling back repaint()
    private void gameDraw(Graphics g) {
        //draw game objects
        snake.draw(g);
        fruit.draw(g);


        g.setFont(new Font("Dialog", Font.PLAIN, 14));
        g.setColor(Color.BLACK);
        g.drawString("Snake: (" + snake.getHeadX() + "," + snake.getHeadY() + ")", 5, 25);
        if (state == GameState.GAMEOVER) {
            g.setFont(new Font("Times New Roman", Font.BOLD, 30));
            g.setColor(Color.RED);
            long totalTime = (System.nanoTime() - startTime) / 1000000000;
            g.drawString("YOU LOSE!! Total Time: " + totalTime + " seconds", 100, CANVAS_HEIGHT / 2);
        }
    }

    public void gameKeyPressed(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
                snake.setDirection(Snake.Direction.UP);
                break;
            case KeyEvent.VK_DOWN:
                snake.setDirection(Snake.Direction.DOWN);
                break;
            case KeyEvent.VK_LEFT:
                snake.setDirection(Snake.Direction.LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                snake.setDirection(Snake.Direction.RIGHT);
                break;
        }
    }

    //Helper function, set up menmubar
    private void setupMenuBar() {
        JMenu menu;
        JMenuItem menuItem;


        menuBar = new JMenuBar();
        // First Menu - "Game"
        menu = new JMenu("Game");
        menu.setMnemonic(KeyEvent.VK_G);
        menuBar.add(menu);


        menuItem = new JMenuItem("New", KeyEvent.VK_N);
        menu.add(menuItem);
        menuItem.addActionListener(e -> {
            if (state == GameState.PLAYING || state == GameState.PAUSED) {
                state = GameState.GAMEOVER;
            }
            gameStart();
            control.reset();
        });


        // Help menu
        menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_H);
        menuBar.add(menu);


        menuItem = new JMenuItem("Help Contents", KeyEvent.VK_H);
        menu.add(menuItem);
        menuItem.addActionListener(arg0 -> {
            //String message = """
            //        Arrow keys to change direction
            //        P to pause/resume\s
            //        S to toggle sound on/off\s
            //        """;
            //JOptionPane.showMessageDialog(Main.this, message,
            //        "Instructions", JOptionPane.PLAIN_MESSAGE);


        });


        menuItem = new JMenuItem("About", KeyEvent.VK_A);
        menu.add(menuItem);
        menuItem.addActionListener(e -> JOptionPane.showMessageDialog(Main.this,
                "Snake Hunt Game, using methods learned in CS3 HORTON (JFrame, JLabels, etc)",
                "About", JOptionPane.PLAIN_MESSAGE));
    }


    enum GameState {
        INITIALIZED, PLAYING, PAUSED, GAMEOVER, DESTROYED
    }

    //game contolm pane: start, stop, pause, mute buttons
    public class ControlPanel extends JPanel {
        private static final long serialVersionUID = 1L;
        private final JButton startButton;
        private final JButton stopButton;
        private final JButton muteButton;

        private final JLabel timerLabel;


        //icon for buttons
        private final ImageIcon iconStart = new ImageIcon(getClass().getResource("start.png"), "START");
        private final ImageIcon iconPause = new ImageIcon(getClass().getResource("pause.png"), "PAUSE");
        private final ImageIcon iconSound = new ImageIcon(getClass().getResource("sound.png"), "SOUND ON");
        private final ImageIcon iconMuted = new ImageIcon(getClass().getResource("muted.png"), "MUTED");

        public ControlPanel(String playerName, Snake snake) {
            this.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

            JPanel labelPanel = new JPanel();
            labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
            JLabel usernameLabel = new JLabel("Username: " + playerName);


            //timer on control panel
            timerLabel = new JLabel("Time Elapsed: 0 seconds");

            labelPanel.add(usernameLabel);
            labelPanel.add(timerLabel);
            add(labelPanel);

            //horizontal panel for radio buttons
            JPanel radioPanel = new JPanel();
            radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.Y_AXIS));
            JRadioButton greenThemeButton = new JRadioButton("Watermelon theme");
            JRadioButton orangeThemeButton = new JRadioButton("Peach Theme");
            JRadioButton redThemeButton = new JRadioButton("Berry Theme");
            JRadioButton yellowThemeButton = new JRadioButton("Banana Theme");
            JRadioButton defaultThemeButton = new JRadioButton("Default Theme");

            ButtonGroup colorGroup = new ButtonGroup();
            colorGroup.add(greenThemeButton);
            colorGroup.add(orangeThemeButton);
            colorGroup.add(redThemeButton);
            colorGroup.add(yellowThemeButton);
            colorGroup.add(defaultThemeButton);


            radioPanel.add(greenThemeButton);
            radioPanel.add(orangeThemeButton);
            radioPanel.add(redThemeButton);
            radioPanel.add(yellowThemeButton);
            radioPanel.add(defaultThemeButton);


            greenThemeButton.addActionListener(e -> {
                snake.setColor(Color.GREEN);
                snake.setColorHead(Color.RED);
            });

            orangeThemeButton.addActionListener(e -> {
                Color peachBody = new Color(252, 204, 196);
                Color peachHead = new Color(252, 190, 106);
                snake.setColor(peachBody);
                snake.setColorHead(peachHead);
            });

            redThemeButton.addActionListener(e -> {
                Color berryBody = new Color(130, 88, 135);
                Color berryHead = new Color(246, 0, 80);
                snake.setColor(berryBody);
                snake.setColorHead(berryHead);
            });


            yellowThemeButton.addActionListener(e -> {

                snake.setColor(Color.YELLOW);
                snake.setColorHead(Color.BLACK);
            });


            defaultThemeButton.addActionListener(e -> {
                snake.setColor(Color.BLACK);
                snake.setColorHead(Color.GREEN);
            });
            add(radioPanel);
            //add(greenThemeButton);
            //add(redThemeButton);
            //add(orangeThemeButton);

            startButton = new JButton(iconPause);
            startButton.setToolTipText("Paused");
            startButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
            startButton.setEnabled(true);
            startButton.setRolloverIcon(new ImageIcon(getClass().getResource("pausehover.jpg")));
            add(startButton);


            ImageIcon iconStop = new ImageIcon(getClass().getResource("stop.png"), "STOP");
            stopButton = new JButton(iconStop);
            stopButton.setToolTipText("Stop");
            stopButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
            stopButton.setRolloverIcon(new ImageIcon(getClass().getResource("playhover.jpg")));
            stopButton.setEnabled(true);

            add(stopButton);


            muteButton = new JButton(iconMuted);
            muteButton.setToolTipText("Stop");
            muteButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
            muteButton.setEnabled(true);
            add(muteButton);


            lblScore = new JLabel("Score: 0");
            add(lblScore);


            // clcicker events on buyttons
            startButton.addActionListener(e -> {
                switch (state) {
                    case INITIALIZED:
                    case GAMEOVER:
                        startButton.setIcon(iconPause);
                        startButton.setToolTipText("Pause");
                        gameStart();
                        //To play a specific sound
                        SoundEffect.CLICK.play();
                        score = 0;
                        lblScore.setText("Score: " + score);
                        break;
                    case PLAYING:
                        state = GameState.PAUSED;
                        startButton.setIcon(iconStart);
                        startButton.setToolTipText("Start");
                        //To play a specific sound
                        SoundEffect.CLICK.play();
                        break;
                    case PAUSED:
                        state = GameState.PLAYING;
                        startButton.setIcon(iconPause);
                        startButton.setToolTipText("Pause");

                        //To play a specific sound
                        SoundEffect.CLICK.play();
                        break;
                }
                stopButton.setEnabled(true);
                pit.requestFocus();


            });
            stopButton.addActionListener(e -> {
                state = GameState.GAMEOVER;
                startButton.setIcon(iconStart);
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
                //To play a specific sound
                SoundEffect.CLICK.play();


            });


            muteButton.addActionListener(e -> {
                if (SoundEffect.volume == SoundEffect.Volume.MUTE) {
                    SoundEffect.volume = SoundEffect.Volume.LOW;
                    muteButton.setIcon(iconSound);
                    //To play a specific sound
                } else {
                    SoundEffect.volume = SoundEffect.Volume.MUTE;
                    muteButton.setIcon(iconMuted);
                    //To play a specific sound
                }
                SoundEffect.CLICK.play();
                pit.requestFocus();


            });


        }

        //reset for new game
        public void reset() {
            startButton.setIcon(iconStart);
            startButton.setEnabled(true);
            stopButton.setEnabled(false);
        }
    }

    public class GameBoard extends JPanel implements KeyListener {
        private static final long serialVersionUID = 1L;

        public GameBoard() {
            setFocusable(true); // recieves key-in events
            requestFocus();
            addKeyListener(this);
        }

        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.decode("0X3F919E"));


            gameDraw(g);

        }

        @Override
        public void keyPressed(KeyEvent e) {
            gameKeyPressed(e.getKeyCode());
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }

        @Override
        public void keyTyped(KeyEvent e) {

        }


        public boolean contains(int x, int y) {
            if ((x < 0) || (x >= ROWS)) return false;
            return (y >= 0) && (y < COLUMNS);
        }

    }
}







