import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.Color;
public class Morning extends JPanel{
    JLabel morningTxt1;
    JLabel morningTxt2;


    public Morning() {
        setBackground(Color.ORANGE);
        setLayout(new GridBagLayout());
        morningTxt1 = new JLabel("3 Difficuities");
        morningTxt2 = new JLabel("Easy Medium Hard");

        GridBagConstraints grid = new GridBagConstraints();
        grid.gridx = 0;
        grid.gridy = 0;
        grid.anchor = GridBagConstraints.CENTER;
        add(morningTxt1, grid);
        grid.gridy = 1;
        add(morningTxt2, grid);



        //morningTxt.setBackground(new Color(255,153,0,1));

        //setBorder(BorderFactory.createMatteBorder(10,10,10,10, Color.black));

    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for(int i = 0; i < 100; i ++)
        {
            for(int k = 0; k < 60; k++)
            {
                int x = 20*i;
                int y = 20*k;
                int circleSize = (int)(Math.random()*15);
                g.drawRect(x,y,circleSize,circleSize);
                g.fillRect(x,y,circleSize,circleSize);
                if(circleSize < 5)
                {
                    g.setColor(Color.RED);
                }
                else if(circleSize < 10)
                {
                    g.setColor(Color.YELLOW);
                }
                else
                {
                    g.setColor(Color.MAGENTA);
                }




            }
        }

        g.setColor(Color.ORANGE);
        g.drawRect(175,100,150,50);
        g.fillRect(175,100,150,50);

    }





}