/**
 * @(#)Evening.java
 *
 *
 * @author
 * @version 1.00 2024/4/3
 */

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.Color;
public class Evening extends JPanel {
    JLabel eveningTxt1;
    JLabel eveningTxt2;
    public Evening() {
        setBackground(Color.LIGHT_GRAY);
        setLayout(new GridBagLayout());
        eveningTxt1 = new JLabel("3 difficuities");
        eveningTxt2 = new JLabel("Easy Medium Hard");

        GridBagConstraints grid = new GridBagConstraints();
        grid.gridx = 0;
        grid.gridy = 0;
        grid.anchor = GridBagConstraints.CENTER;
        add(eveningTxt1, grid);
        grid.gridy = 1;
        add(eveningTxt2, grid);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for(int i = 0; i < 25; i ++)
        {
            for(int k = 0; k < 15; k++)
            {
                int x = 20*i;
                int y = 20*k;
                int circleSize = (int)(Math.random()*15);
                g.drawRoundRect(x,y,circleSize,circleSize,circleSize/3,circleSize/3);
                g.fillRoundRect(x,y,circleSize,circleSize,circleSize/3,circleSize/3);
                if(circleSize < 5)
                {
                    g.setColor(Color.BLACK);
                }
                else if(circleSize < 10)
                {
                    g.setColor(Color.BLUE);
                }
                else
                {
                    g.setColor(Color.MAGENTA);
                }




            }
        }
        g.setColor(Color.LIGHT_GRAY);
        g.drawRect(175,100,175,50);
        g.fillRect(175,100,175,50);

    }


}