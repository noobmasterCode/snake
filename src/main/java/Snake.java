import java.awt.*;
import java.util.ArrayList;
import java.util.Random;


public class Snake {


    private static final int INIT_LENGTH = 3; //snake's cells
    // the snake segments that forms the snake
    private final java.util.List<SnakeSegment> snakeSegments = new ArrayList<SnakeSegment>();
    private final Random random = new Random(); // randomly regenerating a snake
    private Color color = Color.BLACK;
    private Color colorHead = Color.GREEN;
    private Snake.Direction direction; // get the current direction of the snake's head
    private boolean dirUpdatePending; //Pending update for a direction change?

    //Regenerate the snake
    public void regenerate() {
        snakeSegments.clear();
        //Randomly generate a snake inside a pit
        int length = INIT_LENGTH; // 3 cells
        int headX = random.nextInt(Main.COLUMNS - length * 2) + length;
        int headY = random.nextInt(Main.ROWS - length * 2) + length;
        direction = Snake.Direction
                .values()[random.nextInt(Snake.Direction.values().length)];
        snakeSegments.add(new SnakeSegment(headX, headY, length, direction));
        dirUpdatePending = false;


    }

    //sets color of snake w/ jradio buttons
    public void setColor(Color color) {
        this.color = color;
    }

    //sets color of snake head w/ jradio button
    public void setColorHead(Color colorHead) {
        this.colorHead = colorHead;
    }

    public void setDirection(Snake.Direction newDir) {
        // Ignore if there is a direction change pending and no 180 degree turn
        if (!dirUpdatePending
                && (newDir != direction)
                && ((newDir == Snake.Direction.UP && direction != Snake.Direction.DOWN)
                || (newDir == Snake.Direction.DOWN && direction != Snake.Direction.UP)
                || (newDir == Snake.Direction.LEFT && direction != Snake.Direction.RIGHT)
                || (newDir == Snake.Direction.RIGHT && direction != Snake.Direction.LEFT
        ))) {
            SnakeSegment headSegment = snakeSegments.get(0);
            int x = headSegment.getHeadX();
            int y = headSegment.getHeadY();
            //add a new segment with zero length as the new head segment
            snakeSegments.add(0, new SnakeSegment(x, y, 0, newDir));
            direction = newDir;
            dirUpdatePending = true; //will be cleared after updated
        }


    }

    public void update() {
        SnakeSegment headSegment = snakeSegments.get(0);
        headSegment.grow();
        dirUpdatePending = false; //can process the key input again
    }

    public void shrink() {
        SnakeSegment tailSegment = snakeSegments.get(snakeSegments.size() - 1);
        tailSegment.shrink();
        if (tailSegment.getLength() == 0) snakeSegments.remove(tailSegment);
    }

    //Get the X,Y coordinate of the cell that contains the snake's head segment
    public int getHeadX() {
        return snakeSegments.get(0).getHeadX();
    }

    public int getHeadY() {
        return snakeSegments.get(0).getHeadY();
    }

    // Returns true if the snake contains the given (x,y) cell, Used in collision dectection
    public boolean contains(int x, int y) {
        for (int i = 0; i < snakeSegments.size(); ++i) {
            SnakeSegment segment = snakeSegments.get(i);
            if (segment.contains(x, y)) return true;


        }
        return false;
    }

    // return true if the snake eats itself
    public boolean eatItself() {
        int headX = getHeadX();
        int headY = getHeadY();
        //eat itself if the headX, headY hits its body segment (4th onwards)
        for (int i = 3; i < snakeSegments.size(); ++i) {
            SnakeSegment segment = snakeSegments.get(i);
            if (segment.contains(headX, headY)) return true;
        }
        return false;
    }

    // Draw itself
    public void draw(Graphics g) {
        g.setColor(color);
        for (int i = 0; i < snakeSegments.size(); ++i) {
            snakeSegments.get(i).draw(g); //draw all the segments
        }


        if (snakeSegments.size() > 0) {
            g.setColor(colorHead);
            g.fill3DRect(
                    getHeadX() * Main.CELL_SIZE,
                    getHeadY() * Main.CELL_SIZE,
                    Main.CELL_SIZE - 1,
                    Main.CELL_SIZE - 1,
                    true
            );
        }
    }

    // debug method
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Snake[dir=" + direction + "\n");
        for (SnakeSegment segment : snakeSegments) {
            sb.append("   ").append(segment).append("\n");
        }
        sb.append("]");
        return sb.toString();
    }


    public enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
}


