import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;


/*
 * This enum encapsulates all the sound effects of a game, so as to separate the
 * sound playing codes from the game codes
 * 1. Define all your sound effect names and the associated wave file
 * 2. To play a specific sound, simply invoke SoundEffect.SOUND_NAME.play()
 * 3. You might optionally invoke the static method SoundEffect.init() to pre-load all
 * the sound files, so that the play is not paused while loading the file for the first time
 * 4. use SoundEffect.volume to mute the sound
 */


//only supports WAV, AU, AIFF (switch to WAV)
public enum SoundEffect {
    DIE("died.wav"), //game over
    EAT("eat.wav"), //eat an food item
    CLICK("click.wav");

    public static Volume volume = Volume.LOW;
    public boolean loop = false;
    private Clip clip;
    private boolean loopStarted;

    //Constructor to construct each element of the enum with its own sound file
    SoundEffect(String soundFileName) {
        try {

            URL url = SoundEffect.class.getResource(soundFileName);

            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);

            clip = AudioSystem.getClip();

            clip.open(audioInputStream);


        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void init() {
        values();
    }

    public void play() {
        if (volume != Volume.MUTE) {
            if (clip.isRunning()) clip.stop(); //stop the player if it is still running
            clip.setFramePosition(0);
            clip.start();
        }
    }


    public void loop() {
        new Thread(new Runnable() {


            @Override
            public void run() {
                while (loop) {
                    if (volume != Volume.MUTE && !clip.isRunning()) {
                        clip.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    try {
                        Thread.sleep(clip.getMicrosecondLength());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).start();
    }


    public void startLoop() {
        if (!loopStarted) {
            loop = true;
            loop();
        }
    }


    public void stopLoop() {
        loop = false;
        clip.loop(0);
    }


    public void stop() {
        clip.stop();
        clip.setFramePosition(0);
    }


    // Nested class for specifying volume
    public enum Volume {
        MUTE, LOW, MEDIUM, HIGH
    }


}

