import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.Color;
public class Day extends JPanel {
    JLabel dayTxt1;
    JLabel dayTxt2;
    public Day() {
        setBackground(Color.CYAN);
        setLayout(new GridBagLayout());
        dayTxt1 = new JLabel("3 Difficuties");
        dayTxt2 = new JLabel("Easy Medium Hard");

        GridBagConstraints grid = new GridBagConstraints();
        grid.gridx = 0;
        grid.gridy = 0;
        grid.anchor = GridBagConstraints.CENTER;
        add(dayTxt1, grid);
        grid.gridy = 1;
        add(dayTxt2, grid);
    }

    public void paintComponent(Graphics g)
    {

        super.paintComponent(g);

        for(int i = 0; i < 100; i ++)
        {
            for(int k = 0; k < 60; k++)
            {
                int x = 20*i;
                int y = 20*k;
                int circleSize = (int)(Math.random()*15);
                g.drawOval(x,y,circleSize,circleSize);
                g.fillOval(x,y,circleSize,circleSize);

                if(circleSize < 5)
                {
                    g.setColor(Color.YELLOW);
                }
                else if(circleSize < 10)
                {
                    g.setColor(Color.GREEN);
                }
                else
                {
                    g.setColor(Color.BLUE);
                }




            }
        }

        g.setColor(Color.CYAN);
        g.drawRect(175,100,150,50);
        g.fillRect(175,100,150,50);

    }


}