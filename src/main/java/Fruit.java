import java.awt.*;
import java.util.Random;


// fruit is placed in pit for snakes to eat
public class Fruit {
    private final Color color = Color.BLUE;
    //for randomly placing the fruit
    private final Random rand = new Random();
    //current fruit location(x,y)
    private int x, y;


    //default constructor
    public Fruit() {
        //place outside the pit, so that it will not be "displayed"
        x = -1;
        y = -1;
    }


    //Regenerate a fruit item.
    public void regenerate() {
        x = rand.nextInt(Main.COLUMNS - 4) + 2;
        y = rand.nextInt(Main.ROWS - 4) + 2;


    }

    //Return the x, y coordinate of the cell that contains this fruit item
    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }


    //Draw itself
    public void draw(Graphics g) {
        g.setColor(color);
        g.fill3DRect(x * Main.CELL_SIZE,
                y * Main.CELL_SIZE,
                Main.CELL_SIZE,
                Main.CELL_SIZE,
                true);
    }


}
